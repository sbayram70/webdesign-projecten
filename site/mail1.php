<?php
 
 $day = date ('H:i');
$tekst = "tekst";

		if ($day >= 6 && $day < 12 ) {
		$tekst =  "Goede morgen!";
		$style = "morning";
}
	elseif ($day >= 12 && $day < 18 ) {
		$tekst =  "Goede Middag!";
		$style = "afternoon";
}
	elseif ($day >= 18 && $day < 24  ){
		$tekst =  "Goede Avond!";
		$style = "evening";
}
	elseif ($day >= 0 && $day < 6 ){
		$tekst = "Goede Nacht!";
		$style = "night";
}
 
 
 
if(isset($_POST['email'])) {
    $email_to = "sbayram700@gmail.com";
    $email_subject = "Website-mail";
	$thanks = "Bedankt voor u mail .";

    function died($error) {
        echo "We are very sorry, but there were error(s) found with the form you submitted. "; 
        echo "These errors appear below.<br /><br />";
        echo $error."<br /><br />";
        echo "Please go back and fix these errors.<br /><br />";
        die();
    }
    if(!isset($_POST['first_name']) ||
        !isset($_POST['last_name']) ||
        !isset($_POST['email']) || 
        !isset($_POST['comments'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
 
    }
    $first_name = $_POST['first_name']; // required
    $last_name = $_POST['last_name']; // required 
    $email_from = $_POST['email']; // required
    $comments = $_POST['comments']; // required

    $error_message = ""; 
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'Uw E-mail adres is niet correct ingevuld.<br />';
  }
 
    $string_exp = "/^[A-Za-z .'-]+$/";
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'Uw Voornaam is niet correct ingevuld.<br />'; 
  }
 
  if(!preg_match($string_exp,$last_name)) {
    $error_message .= 'Uw Achternaam is niet correct ingevuld.<br />';
  }
 
  if(strlen($comments) < 2) {
    $error_message .= 'Uw Bericht is niet correct ingevuld.<br />'; 
  }
 
    $email_message = "Form details below.\n\n";
	
    function clean_string($string) { 
      $bad = array("content-type","bcc:","to:","cc:","href"); 
      return str_replace($bad,"",$string); 
    }
    $email_message .= "First Name: ".clean_string($first_name)."\n"; 
    $email_message .= "Last Name: ".clean_string($last_name)."\n"; 
    $email_message .= "Email: ".clean_string($email_from)."\n"; 
    $email_message .= "Comments: ".clean_string($comments)."\n";
	
$headers = 'From: '.$email_from."\r\n". 
'Reply-To: '.$email_from."\r\n" . 
'X-Mailer: PHP/' . phpversion(); 
@mail($email_to, $email_subject, $email_message, $headers);   
}
?>

<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Sueyman bayram</title>
		<link rel="stylesheet" href="mobiel.css">
		<img class="logo" src="./foto's/sb.png">

	</head>
	<body>
	<?php echo "<p class=\"tijd\"> $tekst $day "?>
	
	<div class="menu">
			<ul>
				<a href="index.php"><li>Home</li></a>
				<a href="mobielhtml.php"><li>HTML</li></a>
				<a href="mobielphp.php"><li>PHP</li></a>
				<a href="mobielcontact.php"><li>Contact</li></a>
			</ul>
		</div>
		<div class="container">
			<h1> Contactgegevens </h1>
			<hr>

			<a href="https://www.davinci.nl/">
				<img class="dvc" src="./foto's/dvc.jpe"></a>
				<ul class="gegevens">
					<p>School: Da vinci College </p>
					<p>Adres: Leerparkpromenade 100 </p>
					<p>Postcode / Woonplaats: 3312 KW Dordrecht </p>
				</ul>
				<div class="panel">
				
				<ul class="ulpanel"><li><?php echo $error_message; ?></li></ul>
				<?php				
				  if(strlen($error_message) > 0) {
	
				  }
				  else{
				echo $thanks;
				
				}
				?>
				</div>
			
				<div class="socialmedia">


					<a href="https://www.facebook.com/">
					<img class="facebook" src="./foto's/fb.png"></a>
					
					<a href="https://twitter.com/?lang=nl">
					<img class="twitter" src="./foto's/twitter.png"></a>		
					
					<a href="https://instagram.com/">
					<img class="instagram" src="./foto's/insta.png"></a>
					

				
				</div>

		</div>
		<div class="voettekst">
		<p class="tekst1">Deze website is gemaakt door Suleyman Bayram.</p>
		</div>
	</body>
</html>