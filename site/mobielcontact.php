<?php

$day = date ('H:i');
$tekst = "tekst";

		if ($day >= 6 && $day < 12 ) {
		$tekst =  "Goede morgen!";
		$style = "morning";
}
	elseif ($day >= 12 && $day < 18 ) {
		$tekst =  "Goede Middag!";
		$style = "afternoon";
}
	elseif ($day >= 18 && $day < 24  ){
		$tekst =  "Goede Avond!";
		$style = "evening";
}
	elseif ($day >= 0 && $day < 6 ){
		$tekst = "Goede Nacht!";
		$style = "night";
}
	
?>

<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Suleyman Bayram</title>
		<link rel="stylesheet" href="mobiel.css">
		<meta name="viewport" content="width=device-width, initial-scale=1	.0">
		<img class="logo" src="./foto's/sb.png">
	</head>
	<body>
	
	<?php echo "<p class=\"tijd\"> $tekst $day "?>

		<div class="menu">
			<ul>
				<a href="index.php"><li>Home</li></a>
				<a href="mobielhtml.php"><li>HTML</li></a>
				<a href="mobielphp.php"><li>PHP</li></a>
				<a href="mobielcontact.php"><li>Contact</li></a>
			</ul>
		</div>
		<div class="container">
			<h1> Contactgegevens </h1>
			<hr>
			<a href="https://www.davinci.nl/">
				<img class="dvc" src="./foto's/dvc.jpe"></a>
				<ul class="gegevens">
					<p>School: Da Vinci College </p>
					<p>Adres: Leerparkpromenade 100 </p>
					<p>Postcode: 3312 KW, Dordrecht </p>
					
				</ul>
	
			<form name="contactform" method="post" action="mail1.php">
 
				<table class="tabel">
					<tr>
						<td valign="top">
							<label for="first_name">Naam:</label>
						</td>
						<td valign="top"> 
							<input type="text" name="first_name" maxlength="50" size="24" required>
						</td> 
					</tr>
					<tr> 
						<td valign="top"> 
							<label for="last_name">Achternaam: </label>
						</td>
						<td valign="top"> 
							<input  type="text" name="last_name" maxlength="50" size="24" required> 
						</td> 
					</tr>
					<tr> 
						<td valign="top">
							<label for="email">E-mail-Adres:</label> 
						</td>
						<td valign="top">
							<input  type="text" name="email" maxlength="80" size="24" required>
						</td> 
					</tr> 
					<tr>

						<td valign="top"> 
							<label for="comments">Bericht:</label> 
						</td> 
						<td valign="top">
							<textarea  name="comments" maxlength="1500" cols="27" rows="6"></textarea> 
						</td> 
					</tr>
					<tr>
						 <td colspan="2"> 
							<input class="verzendknop" type="submit" value="Verzenden">
						  </td> 
					</tr> 
					
				</table>
			</form>
				<div class="socialmedia">


					<a href="https://www.facebook.com/">
					<img class="facebook" src="./foto's/fb.png"></a>
					
					<a href="https://twitter.com/?lang=nl">
					<img class="twitter" src="./foto's/twitter.png"></a>		
					
					<a href="https://instagram.com/">
					<img class="instagram" src="./foto's/insta.png"></a>
					
				</div>

		</div>
		<div class="voettekst">
			<p class="tekst1">Deze website is gemaakt door Suleyman Bayram.</p>
		</div>
	</body>
</html>